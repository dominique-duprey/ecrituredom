// https://www.browsersync.io/docs/api
// https://github.com/Browsersync/recipes/tree/master/recipes/server

// Pour recevoir les arguments (par ex. de : `nodemon ./server.js build localhost 80`)
// https://nodejs.org/docs/latest/api/process.html#process_process_argv

// Ne pas oublier de lancer un premier build avant de lancer le serveur

process.argv.forEach((index, value) => {
  // console.log('index: %s, value: %s', index, value)
})

const browserSync = require('browser-sync').create();
const nodemon = require('nodemon');
const directory = process.argv[2] ? process.argv[2] : 'build';
const port = process.argv[3] ? process.argv[3] : 3000;

/**
 * Serveur local
 *
 * Installation :
 * ~~~
 * npm i --save-dev nodemon
 * npm i --save-dev browser-sync
 * ~~~
 *
 * Les paramètres de repertoire à surveiller et numéro de port peuvent
 * être passés au démarrage de la manière suivante :
 * ~~~
 * npm start <directory> <port>
 * ~~~
 * ou
 * ~~~
 * nodemon ./server.js <dir> <port>
 * ~~~
 */
nodemon({
  script: 'build.js' // build au démarrage ici
  // ext: 'js json' // extensions dans le fichier de configuration nodemon
});
const { exec } = require('child_process');

exec('node build.js', (err ,stdout, stderr) => {
  browserSync.init({
    server: directory,
    port: port,
    open: false
  });

});
nodemon.on('start', function () {
  // Démarrage de browser sync
  console.log('====================');
  console.log('Démarrage du serveur');
  console.log('Sur le repertoire "%s" et le port %s', directory, port)
  console.log('====================');
  exec("osascript -e 'display notification \"http://localhost:3000\" with title \"Nodemon\" subtitle \"Le serveur local est démarré\" sound name \"Purr\"'");
}).on('quit', function () {
  // Arreter Brwosersync là
  browserSync.exit();
  console.log('App has quit');
  exec("osascript -e 'display notification \"http://localhost:3000\" with title \"Nodemon\" subtitle \"Le serveur local est éteint\" sound name \"Purr\"'");
  process.exit();
}).on('restart', function (files) {
  console.log('Redémarrage du à : ', files);
  browserSync.reload();
  exec("osascript -e 'display notification \"http://localhost:3000\" with title \"Nodemon\" subtitle \"Le serveur local est redémarré\" sound name \"Purr\"'");
  console.log('reconstruction et redemarrage du serveur.');
  // Rebuild, puis recharger browsersync
});


// nodemon --delay 2500ms --exec 'npm run development && npm run server:reload' --ext css,js,mustache,md --watch templates --watch assets --watch content

// https://github.com/remy/nodemon/blob/master/doc/sample-nodemon.md

// Rebuild et recharger le serveur

// Recharger que lors de changement dans les fichiers, pas au démarrage :

// -C, --on-change-only ..... execute script on change only, not startup

// Ou comme module : https://github.com/remy/nodemon/blob/HEAD/doc/requireable.md
