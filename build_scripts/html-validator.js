  const validator = require('html-validator')
  const fs = require('fs')
  const path = require('path');
  const options = {
  format: 'json',
  }


/**
 * Recursively list files filter by extension white list
 *
 * @param {String} directory
 * @param {Array} extensions
 * @param {Array} files
 */
const scanFiles = function scanFiles(directory, extensions) {
  return fs.readdirSync(directory)
    .reduce((files, file) => {

      const filename = path.join(
        directory,
        file
      );

      if (fs.statSync(filename).isDirectory()) {
        return files.concat(scanFiles(
          filename,
          extensions
        ));
      }

      if (! extensions.includes(path.extname(filename))) {
        return files;
      }

      return files.concat(filename);

    }, [] );
};


try {
  scanFiles('./build', ['.html']).map(file => {
    options.data = fs.readFileSync(file, 'utf8');
    validator(options).then(result => {
      const f = file;
      console.log('Fichier : %s', f);
      console.log(result);
      if (result.messages.length)
      {
        for (message of result.messages)
        {
          console.log('---');
          console.log('%s %s', message.type, message.subType ? message.subType : '');
          console.log('ligne : %s - colonne: %s', message.lastLine, message.lastColumn);
          console.log(message.message);
          console.log(message.extract);
        }
        console.log('---');
        process.exit(0);
      }
    }, reject => {

      console.log(reject)
    })
  })
} catch (error) {
  console.error(error)
  process.exit(1);
}
